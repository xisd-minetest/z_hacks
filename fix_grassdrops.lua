--
-- Fix dirt, grass and junglegrass drops of seeds from crops and default
-- and beans from magicbeans_w
--

local items_table = {
			dirt 		= {},
			grass 		= {},
			junglegrass = {},
	}
	
if minetest.get_modpath("magicbeans_w") then
	-- in grass
	table.insert(items_table.grass,{items = {'magicbeans_w:jumping'},rarity = 99})
	table.insert(items_table.grass,{items = {'magicbeans_w:flying'},rarity = 99})
	table.insert(items_table.grass,{items = {'magicbeans_w:running'},rarity = 99})
	table.insert(items_table.grass,{items = {'magicbeans_w:beanstalk'},rarity = 99})
	-- in jungle grass
	table.insert(items_table.junglegrass,{items = {'magicbeans_w:jumping'},rarity = 99})
	table.insert(items_table.junglegrass,{items = {'magicbeans_w:flying'},rarity = 99})
	table.insert(items_table.junglegrass,{items = {'magicbeans_w:running'},rarity = 99})
	table.insert(items_table.junglegrass,{items = {'magicbeans_w:beanstalk'},rarity = 99})
end

if minetest.get_modpath("crops") then
	-- in grass
	table.insert(items_table.grass,{items = {'crops:melon_seed'},rarity = 90})		
	table.insert(items_table.grass,{items = {'crops:pumpkin_seed'},rarity = 85})
	table.insert(items_table.grass,{items = {'crops:tomato_seed'},rarity = 75})
--	table.insert(items_table.grass,{items = {'crops:green_bean_seed'},rarity = 95})
		-- in jungle grass
	table.insert(items_table.junglegrass,{items = {'crops:tomato_seed'},rarity = 70})		
	table.insert(items_table.junglegrass,{items = {'crops:melon_seed'},rarity = 84})
	table.insert(items_table.junglegrass,{items = {'crops:green_bean_seed'},rarity = 85})
	table.insert(items_table.junglegrass,{items = {'crops:corn_base_seed'},rarity = 90})
	-- dirt
	table.insert(items_table.dirt,{ items = {'crops:potato'}, rarity = 350 })
end

if minetest.get_modpath("default") then
	table.insert(items_table.dirt,{ items = {'default:dirt'}})
	-- default rarity 5
	table.insert(items_table.grass,{items = {'farming:seed_wheat'},rarity = 65})
	table.insert(items_table.grass,{items = {'default:grass_1'}})
	-- default rarity 8
	table.insert(items_table.junglegrass,{items = {'farming:seed_cotton'},rarity = 60})
	table.insert(items_table.junglegrass,{items = {'default:junglegrass'}})		
end

	
-- drop potatoes when digging in dirt
minetest.override_item("default:dirt_with_grass", {
	drop = {
		items = items_table.dirt,
	}
})


for i = 1, 5 do		
	minetest.override_item("default:grass_"..i, {drop = {
		max_items = 1,
		items = items_table.grass,
	}})
end
	
minetest.override_item("default:junglegrass", {drop = {
	max_items = 1,
	items = items_table.junglegrass,
}})


if minetest.get_modpath("crops") and minetest.get_modpath("magicbeans_w") then
-- Beanpole abm
	local beans = {"jumping","flying","running","beanstalk"}
	minetest.register_abm(
			{nodenames = {"crops:beanpole_plant_base_5"},
			interval = 500,
			chance = 10,
			action = function(pos)
				pos.y = pos.y + 1
				math.randomseed(os.time())
				local j = math.random(4)
				local bean = beans[j]
				minetest.add_item(pos, {name="magicbeans_w:"..bean})
		end,
	})
end

------------------------------------------------------------
-- Quick fixes for IGN* maps compatibility
-- See : https://minetest.ign.fr/
------------------------------------------------------------
-- 1. Fake dirt 
-- 2. Dry soil don't turn back to dirt
-- 3. Yougtrees drop sticks instead of trunks
-- 4. Rope block drop wood
-- 6. TODO: Replace 0Hardened Clay by aliases ?
-- 

--
-- Fake dirt to prevent dirt paths from turning to grass 
--
--~ minetest.register_node("alac_quickfix:dirt", {
	--~ description = "Dirt",
	--~ tiles = {"default_dirt.png"},
	--~ groups = {crumbly = 3, soil = 1},
	--~ drop = 'default:dirt',
	--~ sounds = default.node_sound_dirt_defaults(),
--~ })

--
-- Soil don't turn back to dirt 
-- ( to allow useless but visually realistic big dry fields )
if minetest.get_modpath("farming") then
	minetest.override_item("farming:soil", {
		soil = {
			base = "farming:soil", -- Soil do not turn back to dirt
			dry = "farming:soil",
			wet = "farming:soil_wet"
		}
	})
end

--
-- Youngtrees are droping sticks instead of trunks
if minetest.get_modpath("youngtrees") and
not  minetest.get_modpath("trunks") then
	minetest.register_alias('trunks:twig_1','default:stick')
end

--
-- Rope block is (too) easy to craft but 
-- now it can only be used once 
-- ( trying to preserve some balance )
--~ if minetest.get_modpath("vines") then
	--~ local ropelike = {}
	--~ if minetest.get_modpath("farming") then
		--~ ropelike = {
				--~ "farming:string",
				--~ "vines:root_middle",							
				--~ "vines:root_end",							
				--~ }
	--~ else
		--~ ropelike = {
				--~ "vines:root_middle",							
				--~ "vines:root_end",							
				--~ }
	--~ end
	--~ minetest.override_item("vines:rope_block", {
		--~ drop = {
			--~ max_items = 2,  -- Maximum number of items to drop.
			--~ items = { -- Choose max_items randomly from this list.
				--~ {
					--~ items = {"default:wood"},  -- Items to drop.
					--~ rarity = 1,  				-- Probability of dropping is 1 / rarity.
				--~ },
				--~ {
					--~ items = ropelike,  -- Items to drop.
					--~ rarity = 2,  		-- Probability of dropping is 1 / rarity.
				--~ },
			--~ },
		--~ },
	--~ })
--~ end

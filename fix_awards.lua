--
-- Fix for some broken awards
-- 
if not  minetest.get_modpath("awards") then return end 

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for MT game translation.
local S = minetest.get_translator(modname)

-- Found a Nyan cat!
-- -- Was broken when nyancat was separated from default
-- -- But this has been fixed since

-- awards.register_achievement("award_nyanfind", {
	-- secret = true,
	-- title = S("A Cat in a Pop-Tart?!"),
	-- description = S("Mine a nyan cat."),
	-- icon = "nyancat_front.png",
	-- trigger = {
		-- type = "dig",
		-- node = "nyancat:nyancat",
		-- target = 1
	-- }
-- })

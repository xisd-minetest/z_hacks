--
-- Window that lets you see trought stone
--
local modname = minetest.get_current_modname()

minetest.register_node(modname..":magic_glass", {
		description = "Magic glass",
		--drawtype = "nodebox",
		tiles = {"magic_window.png"},
		inventory_image = "magic_window.png",
		wield_image = "magic_window.png",
		paramtype = "light",
		paramtype2 = "wallmounted",
		sunlight_propagates = true,
		is_ground_content = false,
		node_box = {
			type = "wallmounted",
			wall_top    = {-0.4375, 0.4375, -0.3125, 0.4375, 0.5, 0.3125},
			wall_bottom = {-0.4375, -0.5, -0.3125, 0.4375, -0.4375, 0.3125},
			wall_side   = {-0.5, -0.3125, -0.4375, -0.4375, 0.3125, 0.4375}
		},
		groups = {choppy = 2, attached_node = 1, oddly_breakable_by_hand = 1},
		legacy_wallmounted = true,
		sounds = default.node_sound_glass_defaults(),
})

minetest.register_craft({
			output = modname..":magic_glass",
			recipe = {
				{'default:glass','default:glass','default:glass'},
				{'default:glass','default:gold_ingot','default:glass'},
				{'default:glass','default:glass','default:glass'}
			}
		})


minetest.register_alias(modname..":magic_window", modname..":magic_glass")

--
-- Merge wool shears from mobs and vine shears from plantlife
-- (need to previously allow sheeps to be sheared by vines:shears)

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for MT game translation.
local S = minetest.get_translator(modname)


-- Override item from vine shears
if minetest.registered_items["vines:shears"] then
	minetest.override_item("vines:shears", {
	  description = S("Shears"),
	  inventory_image = "creatures_shears.png",
	  wield_image = "creatures_shears.png",
	})

	-- Clear the previous recipe for vines shears
	minetest.clear_craft({output = "vines:shears"})
	-- Use mobs recipe instead
	minetest.register_craft({
		output = "vines:shears",
		recipe = {
			{'', 'default:steel_ingot', ''},
			{'', 'group:stick', 'default:steel_ingot'},
		}
	})
end

-- Register alias so that vines:shears is used every time mobs:shears would
minetest.register_alias_force("mobs:shears", "vines:shears")


-- minetest.unregister_item(name)
-- minetest.override_item(name, redefinition)
--[[ 

minetest.register_tool("vines:shears", {
  description = S("Shears"),
  inventory_image = "vines_shears.png",
  wield_image = "vines_shears.png",
  stack_max = 1,
  max_drop_level=3,
  tool_capabilities = {
    full_punch_interval = 1.0,
    max_drop_level=0,
    groupcaps={
      snappy={times={[3]=0.2}, uses=60, maxlevel=3},
      wool={times={[3]=0.2}, uses=60, maxlevel=3}
    }
  },
})


-- shears (right click to shear animal)
minetest.register_tool("mobs:shears", {
	description = S("Steel Shears (right-click to shear)"),
	inventory_image = "mobs_shears.png",
})

minetest.register_craft({
	output = 'mobs:shears',
	recipe = {
		{'', 'default:steel_ingot', ''},
		{'', 'group:stick', 'default:steel_ingot'},
	}
})
--]]

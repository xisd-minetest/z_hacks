--
-- Force longer growth interval for crops

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for intllib.
local S, NS = dofile(modpath.."/intllib.lua")

-- Override crops settings interval
-- Default is 30
if crops and crops.settings then
    crops.settings.interval = 150 
end

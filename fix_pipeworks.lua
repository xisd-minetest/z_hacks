--
-- Add recipe to upgrade some items from pipeworks
--


if minetest.get_modpath("pipeworks") then

    -- Upgrade filter injector (less expensive than crafting a new one)
	minetest.register_craft( {
		output = "pipeworks:mese_filter 2",
		recipe = {
				{ "default:mese_crystal", "default:mese_crystal", "default:mese_crystal" },
				{ "default:mese_crystal", "pipeworks:filter", "default:mese_crystal" },
				{ "default:mese_crystal", "default:mese_crystal", "default:mese_crystal" }
		},
	})

    -- -------------------------------------------------------------- --
    
    -- Autocrafter is like an upgraded craft_bench
    if minetest.registered_items["crafting_bench:workbench"] then
        local b = "crafting_bench:workbench"
        local c = ""
        local m = "default:mese_crystal_fragment"
        if minetest.get_modpath("mesecons") then m = "mesecons:mesecon" end
        
        minetest.register_craft( {
            output = "pipeworks:autocrafter",
            -- type = shapeless,
            -- recipe = { "default:mese_crystal", "default:mese" }
            recipe = {
                    { c, m, c },
                    { m, b, m },
                    { c, m, c }
            },
        })
    end

end

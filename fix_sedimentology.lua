--
-- Sedimentology mod use name default:cobble instead of default:cobblestone

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for MT game translation.
local S = minetest.get_translator(modname)

if minetest.registered_items["default:cobble"]  and ( not minetest.registered_items["default:cobblestone"] ) then

    minetest.register_node(":default:cobblestone", minetest.registered_items["default:cobble"])
    minetest.register_alias("default:cobblestone", "default:cobble")
    
end

--
-- Most of theses so called fixes are in fact
-- tweaks and hacks and compatibility patches
-- set here to avoid editing the original mods
-- 

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath.."/fix_grassdrops.lua")
dofile(modpath.."/fix_pipeworks.lua")
dofile(modpath.."/fix_dye_colors.lua")
dofile(modpath.."/fix_shears.lua")
dofile(modpath.."/fix_bugnet.lua")
--~ dofile(modpath.."/fix_awards.lua")
dofile(modpath.."/fix_crops.lua")
dofile(modpath.."/fix_gates.lua")
dofile(modpath.."/fix_warps.lua")
dofile(modpath.."/fix_alac.lua")
dofile(modpath.."/fix_sedimentology.lua")

-- This was exploiting a bug to create 
-- a "window" that allow to see through cave walls
-- to find were other caves are
-- ... Sadly the bug has been fixed :)
-- dofile(modpath.."/magic_window.lua")

-- This has been moved to fix_sedimentology file
-- minetest.register_alias('defaut:cobblestone', 'defaut:cobble')

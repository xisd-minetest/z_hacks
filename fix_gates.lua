--
-- Add build recipe for the gate controller
--
-- (Also added textures to override default ones)

if minetest.get_modpath("gates") then

	local item_pwr="default:mese_crystal"
	local item_gear="default:tin_ingot"
	local item_stone="default:stonebrick"

	if minetest.get_modpath("pipeworks") then
		item_gear="pipeworks:gear"
	end
	
	if minetest.get_modpath("mesecons_walllever") then
		item_pwr="group:mesecon_conductor_craftable"
		local item_lever = "mesecons_walllever:wall_lever_off"
		minetest.register_craft({
			type = "shapeless",
			output = "gates:controler",
			recipe = { item_stone, item_lever },
		})
	end

	minetest.register_craft({
		output = "gates:controler 2",
		recipe = {
				{ item_stone, "default:stick", item_stone },
				{ item_gear, item_pwr, item_gear },
				{ item_stone, item_stone, item_stone }
		},
	})
end

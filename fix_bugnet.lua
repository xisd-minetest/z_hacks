--
-- Merge net from mobs and bugnet from firefly
-- 

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for MT game translation.
local S = minetest.get_translator(modname)

print('loading fix for bugnet')

-- Override item fireflies:bug_net
if minetest.registered_items["fireflies:bug_net"] and minetest.registered_items["mobs:net"] then
	
    -- Make mobs net look and act like bug net
    minetest.override_item("mobs:net", {
        description = S("Net"),
        inventory_image = "fireflies_bugnet.png",
        on_use = minetest.registered_items["fireflies:bug_net"].on_use,
	})

	-- Use the recipe for fireflies:bug_net
    -- local recipe = minetest.get_craft_recipe("fireflies:bug_net")
    minetest.clear_craft({output = "mobs:net"})
    minetest.register_craft( {
        output = "fireflies:bug_net",
        recipe = {
            {"farming:string", "farming:string"},
            {"farming:string", "farming:string"},
            {"group:stick", ""}
        }
 })

end
-- Register alias so that mobs:net is used whenever firefly:bug_net would
minetest.register_alias_force("fireflies:bug_net","mobs:net")

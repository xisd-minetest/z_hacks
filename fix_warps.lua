--
-- Prevent player without requiered priv to dig warpstones
-- 

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for MT game translation.
local S = minetest.get_translator(modname)

-- Add a can_dig condition 
if minetest.registered_items["warps:warpstone"] then
	
	minetest.override_item("warps:warpstone", {
		can_dig = function(pos, player) 
			if player:get_player_control().sneak then
				local meta = minetest.get_meta(pos)
				local destination = meta:get_string("warps_destination")
				
				if destination == "" then 
					return true 
				elseif minetest.check_player_privs(player:get_player_name(), {warp_admin = true}) then
					--~ minetest.remove_node(pos)
					return true
				else 
					minetest.chat_send_player(player:get_player_name(), "You are not allowed to remove warp stones !")
				end
			end
			return false
		end
	})

end


--
-- Register aliases for some mods that are using too many dye colors
--

if minetest.get_modpath("dye") then 

  minetest.register_alias("dye:lightgrey" ,"dye:grey")
  minetest.register_alias("dye:lime" ,"dye:green")
  minetest.register_alias("dye:aqua", "dye:blue")
  minetest.register_alias("dye:sky_blue" ,"dye:cyan")
  minetest.register_alias("dye:red_violet" ,"dye:magenta")

 end
